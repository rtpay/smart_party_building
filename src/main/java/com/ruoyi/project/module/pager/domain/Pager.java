package com.ruoyi.project.module.pager.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 试卷对象 tb_pager
 * 
 * @author ruoyi
 * @date 2020-06-05
 */
public class Pager extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 试卷名称 */
    @Excel(name = "试卷名称")
    private String name;

    /** 考试对象 */
    @Excel(name = "考试对象")
    private String userId;

    /** 试卷开始时间 */
    @Excel(name = "试卷开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 试卷结束时间 */
    @Excel(name = "试卷结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 考试时间(分钟) */
    @Excel(name = "考试时间(分钟)")
    private Long ksTime;

    /** 总分 */
    @Excel(name = "总分")
    private Long score;

    /** 合格分数 */
    @Excel(name = "合格分数")
    private Long passScore;

    /** 是否发布 */
    @Excel(name = "是否发布")
    private Integer publish;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    private String questionIds;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setKsTime(Long ksTime) 
    {
        this.ksTime = ksTime;
    }

    public Long getKsTime() 
    {
        return ksTime;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setPassScore(Long passScore) 
    {
        this.passScore = passScore;
    }

    public Long getPassScore() 
    {
        return passScore;
    }
    public void setPublish(Integer publish) 
    {
        this.publish = publish;
    }

    public Integer getPublish() 
    {
        return publish;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }


    public String getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(String questionIds) {
        this.questionIds = questionIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("userId", getUserId())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("ksTime", getKsTime())
            .append("score", getScore())
            .append("passScore", getPassScore())
            .append("publish", getPublish())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
